terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}

provider "aws" {
  region = "us-east-1"
}

resource "aws_s3_bucket" "stagging_bucket" {
  bucket        = "puzzle-game-stagging-bucket"
  force_destroy = true
}
resource "aws_s3_bucket_website_configuration" "stagging_bucket_config" {
  bucket = aws_s3_bucket.stagging_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document { 
    key = "index.html"
  }

}
resource "aws_s3_bucket_acl" "stagging_bucket_acl" {
  bucket = aws_s3_bucket.stagging_bucket.id
  acl    = "public-read"

}




resource "aws_s3_bucket" "deployment_bucket" {
  bucket        = "puzzle-game-deployment-bucket"

}
resource "aws_s3_bucket_website_configuration" "deployment_bucket_config" {
  bucket = aws_s3_bucket.deployment_bucket.bucket

  index_document {
    suffix = "index.html"
  }

  error_document { 
    key = "index.html"
  }

routing_rule {
    condition {
      key_prefix_equals = "docs/"
    }
    redirect {
      replace_key_prefix_with = "documents/"
    }
  }

}
resource "aws_s3_bucket_acl" "deployment_bucket_acl" {
  bucket = aws_s3_bucket.deployment_bucket.id
  acl    = "public-read"

}
